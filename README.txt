The types of components that you can select for fields are:
textfield
time
number
email
date

The types of components that you can select for registration types are:
select
product_list

Requirements:

wkhtmltopdf is a requirement for this module. Although this module is heavily based on the print module, and though the print module gives you options of which PDF tool to use, uc_webform_badges gives you no such options.

