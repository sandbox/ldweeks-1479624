<?php

/**
 * @file
 * Default uc_webform_badges module template
 *
 * @ingroup uc_webform_badges
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $badges['language']; ?>" xml:lang="<?php print $badges['language']; ?>">
  <head>
    <title><?php print $badges['title']; ?></title>
    <?php print $badges['css']; ?>
  </head>
  <body>
		<?php print $badges['badge_data']; ?>
  </body>
</html>
